local function get_table_length(table)
  local count = 0
  for _, __ in pairs(table) do
    count = count + 1
  end
  return count
end

local function refresh_marks(buf, ns, line_count)
  local all_marks = vim.api.nvim_buf_get_extmarks(
    buf,
    ns,
    { 1, 1 },
    { line_count, 0 },
    {}
  )
  -- vim.api.nvim_buf_del_extmark(buf, ns, )
  for index, value in ipairs(all_marks) do
    vim.api.nvim_buf_set_extmark(buf, ns, index - 1, 0, {})
  end
end

local function delete_all_marks(buf, ns, line_count)
  local all_marks = vim.api.nvim_buf_get_extmarks(
    buf,
    ns,
    { 1, 1 },
    { line_count, 0 },
    {}
  )
  for _, value in ipairs(all_marks) do
    vim.api.nvim_buf_del_extmark(buf, ns, value[1])
  end
end

local function init_marks(buf, ns, buf_lines)
  for index, value in ipairs(buf_lines) do
    vim.api.nvim_buf_set_extmark(buf, ns, index - 1, 0, {
      virt_text = { { "XXX " .. index, "something_random" } },
    })
  end
end

local function get_marks_at_line(buf, ns, buf_lines, target_line)
  local line = buf_lines[target_line]
  local col
  if line ~= nil then
    col = #line
  else
    col = 0
  end
  return vim.api.nvim_buf_get_extmarks(
    buf,
    ns,
    { target_line, 0 },
    { target_line, col },
    {}
  )
end

local function process_deleted_lines(buf, ns)
  local buf_lines = vim.api.nvim_buf_get_lines(buf, 0, -1, true)
  for index, _ in ipairs(buf_lines) do
    local marks = get_marks_at_line(buf, ns, buf_lines, index)

    -- Delete extmarks for deleted lines.
    if #marks > 1 then
      for _, mark_to_del in ipairs(marks) do
        print("MARK TO DEL: ", vim.inspect(mark_to_del[1]))
        vim.api.nvim_buf_get_extmark_by_id(buf, ns, mark_to_del[1], {})
      end
    end
  end
end

local ns = vim.api.nvim_create_namespace("todo")
local buf = vim.fn.bufnr("%")

local cursor_move_id = current_win_id
if cursor_move_id ~= nil then
  vim.api.nvim_del_autocmd(cursor_move_id)
end

cursor_move_id = vim.api.nvim_create_autocmd({ "CursorMoved" }, {
  callback = function()
    local buf_lines = vim.api.nvim_buf_get_lines(buf, 0, -1, true)
    local win = vim.api.nvim_get_current_win()
    local cursor = vim.api.nvim_win_get_cursor(win)
    local marks_here = get_marks_at_line(buf, ns, buf_lines, cursor[1] - 1)

    print("marks: ", vim.inspect(marks_here))
  end,
})

vim.keymap.set("n", "tp", function()
  local buf_lines = vim.api.nvim_buf_get_lines(buf, 0, -1, true)
  -- delete_all_marks(buf, ns, #buf_lines)
  process_deleted_lines(buf, ns)
end, {
  buffer = buf,
})

vim.keymap.set("n", "tc", function()
  local buf_lines = vim.api.nvim_buf_get_lines(buf, 0, -1, true)
  delete_all_marks(buf, ns, #buf_lines)
end, {
  buffer = buf,
})

vim.keymap.set("n", "ti", function()
  local buf_lines = vim.api.nvim_buf_get_lines(buf, 0, -1, true)
  init_marks(buf, ns, buf_lines)
end, {
  buffer = buf,
})

vim.keymap.set("n", "R", ":w | LuaRun<CR>", {})
