-- TODO: Handle nested constructs (i.e. VALARM, etc.)
-- Build a table of task items and return it.
local function parse_todos(cal_data)
  local todos = {}
  local cur_todo = nil
  for _, value in ipairs(cal_data) do
    if value == 'BEGIN:VTODO' then
      cur_todo = {}
    elseif value == 'END:VTODO' then
      table.insert(todos, cur_todo)
      cur_todo = nil
    end
    if cur_todo ~= nil then
      local pairs = vim.fn.split(value, ':')
      cur_todo[pairs[1]] = pairs[2]
    end
  end
  return todos
end

local cal_data = vim.fn.readfile('./output.xml')
print(vim.inspect(parse_todos(cal_data)))
